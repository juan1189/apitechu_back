var user_file = require('./user.json');

const URL_BASE = '/techu-peru/v1/';
const PATH_DIR = './user.json';

function writeUserDataToFile(data){
  var fs = require('fs');
  var data_json = JSON.stringify(data);

  fs.writeFile(PATH_DIR, data_json,"utf8", function (err) {
    if (err) {
      console.log(err);
    }else{
      console.log("Datos escritos en: ", PATH_DIR);
    }
    });
}

module.exports.cGlobal = {
  URL_BASE,
  user_file,
  writeUserDataToFile
}
