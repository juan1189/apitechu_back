var g = require('../global.js');
var request = require('request-json');
var httpClient = request.createClient('https://api.mlab.com/api/1/databases/techu34db/collections/');

const account_service = require('../services/account_service.js');
const user_service = require('../services/user_service.js');

const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';

function listAccountsByUser(req,res){

  let iduser = req.params.id;

  user_service.getUserById(iduser)
  .then(content => account_service.listAccountsByIdUser(content.response.id)
                   .then(function(content){
                        res.status(content.codeStatus).send(content.response);
                    }))
  .catch(function(err){
    res.status(err.codeStatus).send(err.response);
  })

}

function getAccounts(req , res){

  account_service.getAccounts()
  .then(function(content){
    res.status(content.codeStatus).send(content.response);
  })
  .catch(err => console.log("Hubo un error: " + err));
}

module.exports.listAccountsByUser = listAccountsByUser;

module.exports.getAccounts = getAccounts;
// module.exports.getAccountById = getAccountById;
// module.exports.createAccount = createAccount;
// module.exports.updateAccount = updateAccount;
// module.exports.deleteAccountById = deleteAccountById;
