const AuditoriaService = require(process.cwd() + '/services/auditoria_service.js');

function loginUsuario(req, res){
  var objLogin = req.body;
  console.log('path: ' , process.cwd() + '/services/auditoria_service.js');
  console.log('login controller: ', JSON.stringify(objLogin));
  var auditoriaService = new AuditoriaService();

  auditoriaService.loginUsuario(objLogin)
  .then(function(content){
    res.status(content.codeStatus).send(content.response);
  })
  .catch(function(err){
    res.status(err.codeStatus).send(err.response);
  })
}

function logoutUsuario(req, res){
  var idUsuario = req.params.id;
  var bLogout = false;

  g.cGlobal.user_file.map(function(user){
    if(user.id == idUsuario){
      delete user.logged
      bLogout = true;
    }
    return user;
  });

  g.cGlobal.writeUserDataToFile(g.cGlobal.user_file);

  if(bLogout){
    res.status(201).send({"mensaje":"Logout correcto","id":idUsuario});
  }else{
    res.status(201).send({"mensaje":"Logout incorrecto"});
  }
}

module.exports.loginUsuario = loginUsuario;
module.exports.logoutUsuario = logoutUsuario;
