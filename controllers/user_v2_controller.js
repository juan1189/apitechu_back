// var g = require('../global.js');
const UserService = require('../services/user_service.js');

function getUsers(req, res){
  var user_service = new UserService();

  user_service.getUsers()
  .then(function(content){
    res.status(content.codeStatus).send(content.response);
  })
  .catch(function(err){
    res.status(err.codeStatus).send(err.response);
  })

}

var getUserById = function(req,res){
   var user_service = new UserService();
   let id = req.params.id;

    user_service.getUserById(id)
   .then(function(content){
        res.status(content.codeStatus).send(content.response);
    })
    .catch(function(err){
      res.status(err.codeStatus).send(err.response);
    })
}

var createUser = function(req,res){
  var user_service = new UserService();

  let userNew = { "first_name":req.body.fname,
                  "last_name":req.body.lname,
                  "email":req.body.email,
                  "password":req.body.password};

  user_service.createUser(userNew)
  .then(function(content){
    res.status(content.codeStatus).send(content.response);
  })
  .catch(function(err){
    res.status(err.codeStatus).send(err.response);
  });

}
//
// function createUser(req,res){
//   let userNew = {"id":(g.cGlobal.user_file.length + 1),
//                   "first_name":req.body.fname,
//                   "last_name":req.body.lname,
//                   "email":req.body.email,
//                   "password":req.body.password};
//
//   g.cGlobal.user_file_JSON.push(userNew);
//   g.cGlobal.writeUserDataToFile(g.cGlobal.user_file);
//   res.status(201).send(g.cGlobal.user_file);
// }

var updateUser = function(req, res){
  var user_service = new UserService();

  let userUpdate = req.body;
  userUpdate.id = req.params.id;

  user_service.updateUser(userUpdate)
  .then(function(content){
    res.status(content.codeStatus).send(content.response);
  })
  .catch(function(err){
    res.status(err.codeStatus).send(err.response);
  });
}
//
// function updateUser(req,res){
//
//   let id = req.body.idUsuario;
//
//   g.cGlobal.user_file_JSON.map(function(user){
//     if(user.id == id){
//       user.first_name = req.body.fname,
//       user.last_name = req.body.lname,
//       user.email = req.body.email,
//       user.password = req.body.password
//     }
//     return user;
//   });
//
//   g.cGlobal.writeUserDataToFile(g.cGlobal.user_file);
//   res.status(201).send(g.cGlobal.user_file);
// }

function deleteUserById(req,res){
  let id = req.params.id;
  var user_service = new UserService();

  user_service.deleteUserById(id)
  .then(function(content){
    res.status(content.codeStatus).send(content.response);
  })
  .catch(function(err){
    res.status(err.codeStatus).send(err.response);
  });
  // var index = g.cGlobal.user_file.map(x => {
  //   return Number(x.id);
  // }).indexOf(Number(id));
  //
  // g.cGlobal.user_file_JSON.splice(index,1);
  // g.cGlobal.writeUserDataToFile(g.cGlobal.user_file);
  // res.status(201).send(g.cGlobal.user_file);
}

module.exports.getUsers = getUsers;
module.exports.getUserById = getUserById;
module.exports.createUser = createUser;
module.exports.updateUser = updateUser;
module.exports.deleteUserById = deleteUserById;
