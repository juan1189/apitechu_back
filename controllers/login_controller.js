var express = require('express');
var app = express();
var g = require('../global.js');

function loginUsuario(req, res){
  var correo = req.body.email;
  var clave = req.body.password;
  var bLogin = false;
  var idUsuario = 0;

  g.cGlobal.user_file.map(function(user){
    if(user.email == correo){
      user.logged = true;
      bLogin = true;
      idUsuario = user.id;
    }
    return user;
  });

  g.cGlobal.writeUserDataToFile(g.cGlobal.user_file);

  if(bLogin){
    res.status(201).send({"mensaje":"Login correcto","id":idUsuario});
  }else{
    res.status(201).send({"mensaje":"Login incorrecto"});
  }
}

function logoutUsuario(req, res){
  var idUsuario = req.params.id;
  var bLogout = false;

  g.cGlobal.user_file.map(function(user){
    if(user.id == idUsuario){
      delete user.logged
      bLogout = true;
    }
    return user;
  });

  g.cGlobal.writeUserDataToFile(g.cGlobal.user_file);

  if(bLogout){
    res.status(201).send({"mensaje":"Logout correcto","id":idUsuario});
  }else{
    res.status(201).send({"mensaje":"Logout incorrecto"});
  }
}

module.exports.loginUsuario = loginUsuario;
module.exports.logoutUsuario = logoutUsuario;
