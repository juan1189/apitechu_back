var express = require('express');
var app = express();
var g = require('../global.js');

function getUsers(req, res){
    console.log("query param: ", req.query.id);
    res.status(200).send(g.cGlobal.user_file);
}

var getUsersById = function(req,res){
   let id = req.params.id;

   if(g.cGlobal.user_file.length < id){
     res.status(203).send({"error-code":"E1","error-msg":"Usuario no encontrado"});
   }else if(id == undefined || isNaN(id)){
     res.status(500).send({"error-code":"E2","error-msg":"Parametro invalido"});
   }else {
     var user = g.cGlobal.user_file.filter(x => x.id == id);
     res.status(203).send(user);
    }
}
function createUser(req,res){
  let userNew = {"id":(g.cGlobal.user_file.length + 1),
                  "first_name":req.body.fname,
                  "last_name":req.body.lname,
                  "email":req.body.email,
                  "password":req.body.password};

  g.cGlobal.user_file_JSON.push(userNew);
  g.cGlobal.writeUserDataToFile(g.cGlobal.user_file);
  res.status(201).send(g.cGlobal.user_file);
}

function updateUser(req,res){

  let id = req.body.idUsuario;

  g.cGlobal.user_file_JSON.map(function(user){
    if(user.id == id){
      user.first_name = req.body.fname,
      user.last_name = req.body.lname,
      user.email = req.body.email,
      user.password = req.body.password
    }
    return user;
  });

  g.cGlobal.writeUserDataToFile(g.cGlobal.user_file);
  res.status(201).send(g.cGlobal.user_file);
}

function deleteUserById(req,res){
  let id = req.params.id;

  var index = g.cGlobal.user_file.map(x => {
    return Number(x.id);
  }).indexOf(Number(id));

  g.cGlobal.user_file_JSON.splice(index,1);
  g.cGlobal.writeUserDataToFile(g.cGlobal.user_file);
  res.status(201).send(g.cGlobal.user_file);
}


module.exports.getUsers = getUsers;
module.exports.getUsersById = getUsersById;
module.exports.createUser = createUser;
module.exports.updateUser = updateUser;
module.exports.deleteUserById = deleteUserById;
