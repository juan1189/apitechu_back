var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
const URL_BASE = '/techu-peru/v1';

//Peticion GET
app.get(URL_BASE, function (req, res) {
  var emp = {"nombre":"juan","empresa":"bbva"}
  res.send(emp);
});

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
