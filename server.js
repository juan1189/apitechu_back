var express = require('express');
// var user_file = require('./user.json');
require('dotenv').config(__dirname + '/.env');
var vGlobal = require('./global.js');
var bodyParser = require('body-parser');//objeto para convertir a json el objeto del body.
var app = express();

const user_controller = require('./controllers/user_v2_controller.js');
const account_controller = require('./controllers/account_controller.js');
const login_controller = require('./controllers/login_v2_controller.js');
const URL_BASE = '/techu-peru/v1/';

if (process.env.AMBIENTE == 'PROD') {
  console.log("DESHABILITANDO CONSOLE");
  console.log = function(){};
}

app.use(bodyParser.json());

app.get(URL_BASE + "users", user_controller.getUsers);
app.get(URL_BASE + "users/:id", user_controller.getUserById);
app.put(URL_BASE + "users/:id", user_controller.updateUser);
app.post(URL_BASE + "users", user_controller.createUser);
app.delete(URL_BASE + "users/:id", user_controller.deleteUserById);

app.post(URL_BASE + "login", login_controller.loginUsuario);
app.post(URL_BASE + "logout/:id", login_controller.logoutUsuario);

// CUENTAS
app.get(URL_BASE + "users/:id/accounts", account_controller.listAccountsByUser);
app.get(URL_BASE + "accounts", account_controller.getAccounts);

// app.listen(process.env.PORT, function() {
//   console.log('Example app listening on port !'),
// });
app.listen(process.env.PORT, function () {
  console.log('Example app listening on port', process.env.PORT);
});
