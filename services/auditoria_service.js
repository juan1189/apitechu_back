var request = require('request-json');

var AuditoriaService = function() {
  this.apikeyMLab = 'apiKey='+process.env['API_KEY'];
  this.httpClient = request.createClient(process.env['PATH_BASE_MLAB']);
}

AuditoriaService.prototype.loginUsuario = function(objLogin){
  console.log('Service => loginUsuario');
  var auditoriaService = this;
  var objResponse = {};
  var queryString = 'q={"email":"'+ objLogin.email +'","password":"'+ objLogin.password +'"}&';

  return new Promise(function(resolve, reject){
    auditoriaService.httpClient.get('user?' + queryString + auditoriaService.apikeyMLab,
      function(err, respuestaMLab, body) {
        if(err) {
            objResponse.response = {"msg" : "Error al obtener usuario: "+ objLogin.email}
            objResponse.codeStatus = 500;
            reject(objResponse);
        }else if(body.length <= 0){
            objResponse.response = {"msg" : "Usuario no encontrado: "+ objLogin.email}
            objResponse.codeStatus = 400;
            reject(objResponse);
        }else{
          let bodyLogin = body[0];
          let queryUpdate = '{"$set":{"logged":true}}';
          let queryString = `q={"id": ${bodyLogin.id} }&`;
          auditoriaService.httpClient.put('user?' + queryString + auditoriaService.apikeyMLab, JSON.parse(queryUpdate),
              function(err, respuestaMLab, body) {
                if(err) {
                    objResponse.response = {"msg" : "Error login usuario: "+ bodyLogin.email}
                    objResponse.codeStatus = 500;
                }else if(body.n != 1){
                    objResponse.response = {"msg" : "Usuario no encontrado: "+ bodyLogin.email}
                    objResponse.codeStatus = 400;
                }else{
                    objResponse.response = {"msg" : "Se logueo con exito: " + bodyLogin.email};
                    objResponse.codeStatus = 200;
                }

              if(objResponse.codeStatus < 300 && objResponse.codeStatus >= 200)resolve(objResponse);
              reject(objResponse);
           });
         }//Fin validacion obtener usuario
      });
  });

}

module.exports = AuditoriaService;
