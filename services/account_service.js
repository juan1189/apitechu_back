var request = require('request-json');
var httpClient = request.createClient('https://api.mlab.com/api/1/databases/techu34db/collections/');

const apikeyMLab = 'apiKey='+process.env['API_KEY'];;

function listAccountsByIdUser(idUser){
  var objResponse = {};
  var queryString = 'q={"iduser": '+idUser+'}&';
  var queryExclude = 'f={"_id": 0}&';
  console.log("Service => GET /account by iduser: ", idUser);

  return new Promise(function(resolve, reject){
    httpClient.get('account?' + queryString + queryExclude + apikeyMLab,
      function(err, respuestaMLab, body) {
        if(err) {
            objResponse.response = {"msg" : "Error obteniendo usuario: "+id}
            objResponse.codeStatus = 500;
        } else {
          if(body.length > 0) {
            console.log(body.length);
            objResponse.response = body;
            objResponse.codeStatus = 200;
          } else {
            objResponse.response = {"msg" : "Ningún elemento 'user' para id: " +id}
            objResponse.codeStatus = 404
          }
        }
        if(objResponse.codeStatus < 300 && objResponse.codeStatus >= 200)resolve(objResponse);
        reject(objResponse);
   });
  });
}

function getAccounts(){
  console.log("Service => GET /accounts");
  var objResponse = {};
  var queryExclude = 'f={"_id": 0}&';

  return new Promise(function(resolve, reject){
    httpClient.get('account?' + queryExclude + apikeyMLab,
      function(err, respuestaMLab, body) {
        if(err) {
            objResponse.response = {"msg" : "Error obteniendo cuentas"}
            objResponse.codeStatus = 500;
        } else {
          if(body.length > 0) {
            console.log(body.length);
            objResponse.response = body;
            objResponse.codeStatus = 200;
          } else {
            objResponse.response = {"msg" : "Ningún elemento 'cuenta'"}
            objResponse.codeStatus = 404
          }
        }
      resolve(objResponse);
   });
  });
}

module.exports.listAccountsByIdUser = listAccountsByIdUser;
module.exports.getAccounts = getAccounts;
