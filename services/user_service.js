var request = require('request-json');

var UserService = function(name) {
  this.apikeyMLab = 'apiKey='+process.env['API_KEY'];
  this.httpClient = request.createClient(process.env['PATH_BASE_MLAB']);
}

UserService.prototype.getUsers = function(){
  console.log("Service => GET /users");

  var userService = this;

  return new Promise(function(resolve,reject){

    var queryString = 'f={"_id":0}&';
    var objResponse = {};
    console.log("apiker: " , userService.apikeyMLab);
    userService.httpClient.get('user?' + queryString + userService.apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            objResponse.response ={"msg" : "Error obteniendo usuarios."}
            objResponse.codeStatus = 500;
        } else {
          if(body.length > 0) {
            objResponse.response = body;
            objResponse.codeStatus = 200;
          } else {
            objResponse.response = {"msg" : "Ningún elemento 'user'"}
            objResponse.codeStatus = 404
          }
        }
        if(objResponse.codeStatus < 300 && objResponse.codeStatus >= 200)resolve(objResponse);
        reject(objResponse);
      });
  });
}

var getUserIdPrimary = function(id){
  console.log("Service Private => GET /users by idPrimary: ", id);

  var userService = new UserService();
  var objResponse = {};
  var queryString = `q={"id": ${id}}&`;
  return new Promise(function(resolve, reject){
    userService.httpClient.get('user?' + queryString + userService.apikeyMLab,
      function(err, respuestaMLab, body) {
        if(err) {
            objResponse.response = {"msg" : "Error obteniendo usuario: "+id};
            objResponse.codeStatus = 500;
        } else {
          if(body.length > 0) {
            objResponse.response = {"_id": body[0]._id.$oid};
            objResponse.codeStatus = 200;
          } else {
            objResponse.response = {"msg" : "Ningún elemento 'user' para id: " +id}
            objResponse.codeStatus = 404
          }
        }
      if(objResponse.codeStatus < 300 && objResponse.codeStatus >= 200)resolve(objResponse);
      reject(objResponse);
   });
  });
}

UserService.prototype.getUserById = function(id){
  console.log("Service => GET /user by id: ", id);
  console.log('dire1: ',__dirname)      // "/Users/Sam/gator-app/src/api"
  console.log('dire2: ',process.cwd())  // "/Users/Sam/gator-app"
  var userService = this;
  var objResponse = {};
  var queryString = `q={"id": ${id}}&`;
  var queryExclude = 'f={"_id": 0}&';

  return new Promise(function(resolve, reject){
    userService.httpClient.get('user?' + queryString + queryExclude + userService.apikeyMLab,
      function(err, respuestaMLab, body) {
        if(err) {
            objResponse.response = {"msg" : "Error obteniendo usuario: "+id}
            objResponse.codeStatus = 500;
        } else {
          if(body.length > 0) {
            objResponse.response = body[0];
            objResponse.codeStatus = 200;
          } else {
            objResponse.response = {"msg" : "Ningún elemento 'user' para id: " +id}
            objResponse.codeStatus = 404
          }
        }
      if(objResponse.codeStatus < 300 && objResponse.codeStatus >= 200)resolve(objResponse);
      reject(objResponse);
   });
  });
}

UserService.prototype.createUser = function(objUser){
  console.log("Service => POST /user");

  var userService = this;
  var objResponse = {};

  return new Promise(function(resolve, reject){
    userService.getUsers()
    .then(function(content){
          var newId = content.response.length + 1;
          let jsonUser = {
            "id" : newId,
            "first_name" : objUser.first_name,
            "last_name" : objUser.last_name,
            "email" : objUser.email,
            "password" : objUser.password
          };

          userService.httpClient.post('user?' + userService.apikeyMLab, jsonUser,
            function(err, respuestaMLab, body) {
              if(err) {
                  objResponse.response = {"msg" : "Error al guardar usuario: "+ objUser.email}
                  objResponse.codeStatus = 500;
              } else {
                  objResponse.response = {"msg" : "Se creo con exito el usuario: " + objUser.email};
                  objResponse.codeStatus = 201;
              }

            if(objResponse.codeStatus < 300 && objResponse.codeStatus >= 200)resolve(objResponse);
            reject(objResponse);
         });
       })
       .catch(function(err){
         reject(err);
       });
  });

}

UserService.prototype.updateUser = function(objUser){
  console.log("Service => PUT /user");

  var userService = this;
  var objResponse = {};
  let idUser = objUser.id;

  return new Promise(function(resolve, reject){
    userService.getUserById(idUser)
    .then(function(content){
          delete objUser.id;
          let queryUpdate = '{"$set":'+ JSON.stringify(objUser) +'}';
          let queryString = `q={"id": ${idUser}}&`;
          userService.httpClient.put('user?' + queryString + userService.apikeyMLab, JSON.parse(queryUpdate),
            function(err, respuestaMLab, body) {
              if(err) {
                  objResponse.response = {"msg" : "Error al actualizar usuario: "+ objUser.email}
                  objResponse.codeStatus = 500;
              } else {
                  objResponse.response = {"msg" : "Se actualizo con exito el usuario: " + objUser.email};
                  objResponse.codeStatus = 200;
              }

            if(objResponse.codeStatus < 300 && objResponse.codeStatus >= 200)resolve(objResponse);
            reject(objResponse);
         });
       })
       .catch(function(err){
         reject(err);
       });
  });

}

UserService.prototype.deleteUserById = function(idUser){
  console.log("Service => DELETE /user");

  var userService = this;
  var objResponse = {};

  return new Promise(function(resolve, reject){
    getUserIdPrimary(idUser)
    .then(function(content){
          let idPrimaryUser = content.response._id;

          userService.httpClient.delete('user/' + idPrimaryUser + '?' + userService.apikeyMLab,
            function(err, respuestaMLab, body) {
              if(err) {
                  objResponse.response = {"msg" : "Error al eliminar usuario: "+ idUser}
                  objResponse.codeStatus = 500;
              } else {
                  objResponse.response = {"msg" : "Se elimino con exito el usuario: " + idUser};
                  objResponse.codeStatus = 200;
              }

            if(objResponse.codeStatus < 300 && objResponse.codeStatus >= 200)resolve(objResponse);
            reject(objResponse);
         });
       })
       .catch(function(err){
         reject(err);
       });
  });
}

module.exports = UserService;
